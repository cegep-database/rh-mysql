-- SELECT ------------------------------------------------------------------------------------------------
-- 1) Retrouver et afficher les informations sur les bureaux d’un pays donné si on connait son ID (ex. JP)
SELECT *
	FROM bureau
    WHERE ID_PAYS = 'JP';
    
-- 2) Retrouver et afficher les informations sur les bureaux triés selon le ID_PAYS.
SELECT *
	FROM bureau
    ORDER BY ID_PAYS;
    
-- 3) Afficher le ID_PAYS, le nom du pays et le nombre de bureaux de chaque pays.
SELECT
	p.ID_PAYS AS 'ID pays'
    ,p.NOM_PAYS AS 'Nom pays'
    ,COUNT(b.ID_BUREAU) AS 'Nombre de bureaux'
    FROM pays AS p
    INNER JOIN bureau AS b
    ON p.ID_PAYS = b.ID_PAYS
    GROUP BY p.ID_PAYS;
    
-- 4) Afficher le ID_EMPLOI et le TITRE_EMPLOI de l’emploi qui a le plus cher salaire selon salaire_maximum.
SELECT
	ID_EMPLOI AS 'ID Emploi'
    ,TITRE_EMPLOI AS 'Titre emploi'
    ,MAX(SALAIRE_MAXIMUM) AS 'Salaire le plus élevé'
    FROM emploi;
    
-- 5) Afficher le salaire minimum moyen et le salaire maximum moyen des emplois de la compagnie.
SELECT
    ROUND(AVG(SALAIRE_MINIMUM), 2) AS 'Salaire minimum moyen'
    ,ROUND(AVG(SALAIRE_MAXIMUM), 2) AS 'Salaire maximum moyen'
    FROM emploi;
    
-- 6) Afficher le ID_REGION et le nombre de pays de chaque région
SELECT
	r.ID_REGION AS 'ID Région'
    ,COUNT(p.ID_PAYS) AS 'Nombre de pays dans la région'
    FROM region AS r
    INNER JOIN pays AS p
    ON r.ID_REGION = p.ID_REGION
    GROUP BY r.ID_REGION;
    
-- 7) Afficher le nom de la région et le nombre de pays de chaque région
SELECT
	r.NOM_REGION AS 'Région'
    ,COUNT(p.ID_PAYS) AS 'Nombre de pays dans la région'
    FROM region AS r
    INNER JOIN pays AS p
    ON r.ID_REGION = p.ID_REGION
    GROUP BY r.ID_REGION;
    
-- 8) Afficher le nom du pays et nombre de bureaux de chaque pays
SELECT
	p.NOM_PAYS AS 'Pays'
    ,COUNT(b.ID_BUREAU) AS 'Nombre de bureau dans le pays'
    FROM pays AS p
    INNER JOIN bureau AS b
    ON p.ID_PAYS = b.ID_PAYS
    GROUP BY p.ID_PAYS;
    
    
    
-- VUES -------------------------------------------------------------------
-- Afficher le nom de la région et le nom des pays de chaque région
CREATE VIEW Vue1 AS
SELECT r.NOM_REGION, p.NOM_PAYS
FROM region as r
	INNER JOIN pays as p
		ON r.ID_REGION = p.ID_REGION
ORDER BY r.NOM_REGION;

-- Afficher le nom des régions qui possède plus que 3 pays.
CREATE VIEW Vue2 AS
SELECT r.NOM_REGION
FROM region as r
	INNER JOIN pays as p
		ON r.ID_REGION = p.ID_REGION
GROUP BY r.NOM_REGION
HAVING COUNT(P.ID_REGION) >=3
ORDER BY r.NOM_REGION;


-- Afficher pour chaque employé, son nom, prénom, son titre d’emploi actuel, le nom de son département, son salaire et la date de début de son emploi actuel. 
CREATE VIEW Vue3 AS
SELECT  employe.NOM_EMPLOYE, employe.PRENOM_EMPLOYE, 
		emploi.TITRE_EMPLOI, 
		departement.NOM_DEPARTEMENT, 
        historique_emploi.SALAIRE, historique_emploi.DATE_DEBUT_EMPLOI
FROM employe
INNER JOIN historique_emploi ON  employe.ID_EMPLOYE = historique_emploi.ID_EMPLOYE
	INNER JOIN emploi  ON historique_emploi.ID_EMPLOI = emploi.ID_EMPLOI
		INNER JOIN departement ON historique_emploi.ID_DEPARTEMENT = departement.ID_DEPARTEMENT
WHERE historique_emploi.DATE_FIN_EMPLOI IS NULL OR historique_emploi.DATE_FIN_EMPLOI > CURRENT_DATE();


-- Afficher le nom et le salaire moyen des emplois actuels de chaque département
CREATE VIEW Vue4 AS
SELECT  emploi.TITRE_EMPLOI AS "Emploi", 
        ROUND(AVG(historique_emploi.SALAIRE), 2) AS "Salaire Moyen"
FROM emploi
INNER JOIN historique_emploi ON emploi.ID_EMPLOI = historique_emploi.ID_EMPLOI
WHERE historique_emploi.DATE_FIN_EMPLOI IS NULL OR historique_emploi.DATE_FIN_EMPLOI > CURRENT_DATE()
GROUP BY emploi.TITRE_EMPLOI;

 
-- Afficher le nom et la masse salariale de chaque département (les emplois actuels)
CREATE VIEW Vue5 AS
SELECT NOM_DEPARTEMENT as "Departement",
	   SUM(historique_emploi.SALAIRE) AS "Masse Salariale"
FROM historique_emploi
	INNER JOIN departement on departement.ID_DEPARTEMENT = historique_emploi.ID_DEPARTEMENT
WHERE historique_emploi.DATE_FIN_EMPLOI IS NULL OR historique_emploi.DATE_FIN_EMPLOI > CURRENT_DATE()
GROUP BY departement.NOM_DEPARTEMENT;


-- Afficher le nom et la masse salariale de chaque bureau (les emplois actuels)
CREATE VIEW Vue6 AS
SELECT  Bureau.NOM_VILLE_BUREAU as "Nom du bureau",
		SUM(historique_emploi.SALAIRE) AS "Masse Salariale"
FROM Bureau
INNER JOIN departement ON Bureau.ID_BUREAU = departement.ID_BUREAU
	INNER JOIN historique_emploi ON departement.ID_DEPARTEMENT = historique_emploi.ID_DEPARTEMENT
WHERE historique_emploi.DATE_FIN_EMPLOI IS NULL OR historique_emploi.DATE_FIN_EMPLOI > CURRENT_DATE()
GROUP BY Bureau.NOM_VILLE_BUREAU;


-- Afficher le nom des employés qui ont occupé 3 emplois incluant celui qu’ils occupent encore.
CREATE VIEW Vue7 AS
SELECT  DISTINCT CONCAT(employe.PRENOM_EMPLOYE, " ", employe.NOM_EMPLOYE) as "Nom"
FROM employe
INNER JOIN historique_emploi ON employe.ID_EMPLOYE = historique_emploi.ID_EMPLOYE
WHERE employe.ID_EMPLOYE = ANY (SELECT ID_EMPLOYE FROM historique_emploi GROUP BY ID_EMPLOYE HAVING COUNT(ID_EMPLOYE) >=3)
ORDER BY employe.PRENOM_EMPLOYE;


-- Afficher le nom et le prénom de l’employé qui a changé le plus d’emploi.
CREATE VIEW Vue8 AS
SELECT  DISTINCT CONCAT(employe.PRENOM_EMPLOYE, " ", employe.NOM_EMPLOYE) as "Nom"
FROM employe
INNER JOIN historique_emploi ON employe.ID_EMPLOYE = historique_emploi.ID_EMPLOYE
WHERE employe.ID_EMPLOYE = (SELECT ID_EMPLOYE FROM historique_emploi GROUP BY ID_EMPLOYE ORDER BY COUNT(ID_EMPLOYE) DESC LIMIT 1)
ORDER BY employe.PRENOM_EMPLOYE;


-- Afficher le nom (sous forme Nom, Prénom) de chaque employé ainsi que le nom (sous forme Nom, prénom) de son responsable. 
CREATE VIEW Vue9 AS
SELECT CONCAT(parent.NOM_EMPLOYE, " ", parent.PRENOM_EMPLOYE) AS "Employe",
	   CONCAT(employe.NOM_EMPLOYE, " ",employe.PRENOM_EMPLOYE) AS "Responsable"
FROM employe
LEFT JOIN employe AS parent ON employe.ID_EMPLOYE = parent.RESPONSABLE_EMPLOYE
WHERE parent.RESPONSABLE_EMPLOYE = employe.ID_EMPLOYE 
ORDER BY Parent.RESPONSABLE_EMPLOYE;

