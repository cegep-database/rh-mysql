-- Bases de données : Travail Pratique 2
-- Noms des équipiers:

-- Tristan Hamel		Matricule: 2192068
-- Nicolas Lavigne		Matricule: 2191913

-- --------------------------------------------------------------------
-- PARTIE 1 — DDL : création de la base de données avec ses contraintes
-- --------------------------------------------------------------------

DROP DATABASE IF EXISTS HR;
CREATE DATABASE HR;
USE HR;

CREATE TABLE region(
	ID_REGION INT NOT NULL PRIMARY KEY
    ,NOM_REGION VARCHAR(30) NOT NULL
)ENGINE = InnoDB;

CREATE TABLE pays(
	ID_PAYS VARCHAR(3) NOT NULL PRIMARY KEY
    ,NOM_PAYS VARCHAR(30) NOT NULL
    ,ID_REGION INT NOT NULL
    ,CONSTRAINT FK_ID_REGION FOREIGN KEY (ID_REGION) REFERENCES region (ID_REGION)
)ENGINE = InnoDB;

CREATE TABLE bureau(
	ID_BUREAU INT NOT NULL PRIMARY KEY
    ,ADRESSE_RUE_BUREAU VARCHAR(30) NOT NULL
    ,CODE_POSTAL_BUREAU VARCHAR(12) NULL DEFAULT NULL
    ,NOM_VILLE_BUREAU VARCHAR(60) NOT NULL
    ,COURRIEL_BUREAU VARCHAR(60) NOT NULL
    ,ID_PAYS VARCHAR(3) NOT NULL
    ,CONSTRAINT FK_ID_PAYS FOREIGN KEY (ID_PAYS) REFERENCES pays (ID_PAYS)
)ENGINE = InnoDB;

CREATE TABLE employe(
	ID_EMPLOYE INT NOT NULL PRIMARY KEY
    ,PRENOM_EMPLOYE VARCHAR(30) NOT NULL
    ,NOM_EMPLOYE VARCHAR(30) NOT NULL
    ,COURRIEL_EMPLOYE VARCHAR(60) NOT NULL
    ,ADRESSE_RUE_EMPLOYE VARCHAR(60) NULL DEFAULT NULL
    ,ADRESSE_VILLE_EMPLOYE VARCHAR(60) NULL DEFAULT NULL
    ,ADRESSE_CP_EMPLOYE VARCHAR(12) NULL DEFAULT NULL
    ,RESPONSABLE_EMPLOYE INT NULL DEFAULT NULL
    ,CONSTRAINT FK_RESPONSABLE_EMPLOYE FOREIGN KEY (RESPONSABLE_EMPLOYE) REFERENCES employe (ID_EMPLOYE)
)ENGINE = InnoDB;

CREATE TABLE departement(
	ID_DEPARTEMENT INT NOT NULL PRIMARY KEY
    ,NOM_DEPARTEMENT VARCHAR(30) NOT NULL
    ,DIRECTEUR_DEPARTEMENT INT NULL DEFAULT NULL
    ,ID_BUREAU INT NOT NULL
    ,CONSTRAINT FK_DIRECTEUR_DEPARTEMENT FOREIGN KEY (DIRECTEUR_DEPARTEMENT) REFERENCES employe (ID_EMPLOYE)
    ,CONSTRAINT FK_ID_BUREAU FOREIGN KEY (ID_BUREAU) REFERENCES bureau (ID_BUREAU)
)ENGINE = InnoDB;

CREATE TABLE emploi(
	ID_EMPLOI VARCHAR(10) NOT NULL PRIMARY KEY
    ,TITRE_EMPLOI VARCHAR(60) NOT NULL
    ,SALAIRE_MINIMUM DECIMAL(10,2) NOT NULL DEFAULT 100
    ,SALAIRE_MAXIMUM DECIMAL(10,2) NOT NULL
    ,CONSTRAINT CHK_SALAIRE_MINIMUM CHECK (SALAIRE_MINIMUM BETWEEN 100 AND 100000)
    ,CONSTRAINT CHK_SALAIRE_MAXIMUM CHECK (SALAIRE_MAXIMUM BETWEEN 250 AND 250000)
    ,CONSTRAINT CHK_SALAIRE CHECK (SALAIRE_MINIMUM < SALAIRE_MAXIMUM)
)ENGINE = InnoDB;

CREATE TABLE historique_emploi(
	ID_EMPLOYE INT NOT NULL
    ,ID_EMPLOI VARCHAR(10) NOT NULL
    ,DATE_DEBUT_EMPLOI DATETIME NOT NULL
    ,DATE_FIN_EMPLOI DATETIME NULL DEFAULT (TIMESTAMPADD(YEAR, 10, CURRENT_TIMESTAMP))
    ,ID_DEPARTEMENT INT NOT NULL
    ,SALAIRE DECIMAL(10,2) NOT NULL
    ,CONSTRAINT FK_ID_EMPLOYE FOREIGN KEY (ID_EMPLOYE) REFERENCES employe (ID_EMPLOYE)
    ,CONSTRAINT FK_ID_EMPLOI FOREIGN KEY (ID_EMPLOI) REFERENCES emploi (ID_EMPLOI)
    ,CONSTRAINT FK_ID_DEPARTEMENT FOREIGN KEY (ID_DEPARTEMENT) REFERENCES departement (ID_DEPARTEMENT)
    ,CONSTRAINT CHK_DATE CHECK (DATE_DEBUT_EMPLOI < DATE_FIN_EMPLOI OR DATE_FIN_EMPLOI = NULL)
    ,PRIMARY KEY (ID_EMPLOYE, ID_EMPLOI, DATE_DEBUT_EMPLOI)
)ENGINE = InnoDB;

-- -------------------------------------------------------------------------------------------------------------
-- PARTIE 2 — DML : insertions des données ; (les données des fichiers csv sans aucune modification des données)
-- -------------------------------------------------------------------------------------------------------------

-- Pour verifier et faire fonctionner l'importation de CSV
SHOW VARIABLES LIKE "local_infile";
SET GLOBAL local_infile =1;
SHOW VARIABLES LIKE "secure_file_priv";

SET FOREIGN_KEY_CHECKS = 0;

-- REGION ----------------------------------------- 
TRUNCATE TABLE region;

LOAD DATA INFILE 'C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\REGIONS.CSV'
INTO TABLE region
COLUMNS TERMINATED BY ';' 
OPTIONALLY ENCLOSED BY '"' 
ESCAPED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(ID_REGION,NOM_REGION);

-- PAYS ------------------------------------------- 
TRUNCATE TABLE pays;

LOAD DATA INFILE 'C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\PAYS.CSV'
INTO TABLE pays
COLUMNS TERMINATED BY ';' 
OPTIONALLY ENCLOSED BY '"' 
ESCAPED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(ID_PAYS,NOM_PAYS,ID_REGION);

-- BUREAU ----------------------------------------- 
TRUNCATE TABLE bureau;

LOAD DATA INFILE 'C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\BUREAUX.CSV'
INTO TABLE bureau
COLUMNS TERMINATED BY ';' 
OPTIONALLY ENCLOSED BY '"' 
ESCAPED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(ID_BUREAU,ID_PAYS,ADRESSE_RUE_BUREAU,CODE_POSTAL_BUREAU,NOM_VILLE_BUREAU,COURRIEL_BUREAU);

-- EMPLOYE -----------------------------------------  *** PROBLEME AVEC ADRESSE_CP_EMPLOYE NON NULL ??? ***
TRUNCATE TABLE employe;

LOAD DATA INFILE 'C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\EMPLOYES.CSV'
INTO TABLE employe
COLUMNS TERMINATED BY ';' 
OPTIONALLY ENCLOSED BY '"' 
ESCAPED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(ID_EMPLOYE,@RESPONSABLE_EMPLOYE,NOM_EMPLOYE,PRENOM_EMPLOYE,COURRIEL_EMPLOYE,@ADRESSE_RUE_EMPLOYE,@ADRESSE_VILLE_EMPLOYE,@ADRESSE_CP_EMPLOYE)
SET RESPONSABLE_EMPLOYE=nullif(@RESPONSABLE_EMPLOYE,''),
ADRESSE_RUE_EMPLOYE = nullif(@ADRESSE_RUE_EMPLOY,''),
ADRESSE_CP_EMPLOYE = nullif(@ADRESSE_CP_EMPLOYE,''),
ADRESSE_VILLE_EMPLOYE = nullif(@ADRESSE_VILLE_EMPLOYE,'');

-- DEPARTEMENT ----------------------------------------- 
TRUNCATE TABLE departement;

LOAD DATA INFILE 'C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\DEPARTEMENTS.CSV'
INTO TABLE departement
COLUMNS TERMINATED BY ';' 
OPTIONALLY ENCLOSED BY '"' 
ESCAPED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(ID_DEPARTEMENT,NOM_DEPARTEMENT,@DIRECTEUR_DEPARTEMENT,ID_BUREAU)
SET DIRECTEUR_DEPARTEMENT=nullif(@DIRECTEUR_DEPARTEMENT,'');

-- EMPLOI -----------------------------------------
TRUNCATE TABLE emploi; 

LOAD DATA INFILE 'C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\EMPLOIS.CSV'
INTO TABLE emploi
COLUMNS TERMINATED BY ';' 
OPTIONALLY ENCLOSED BY '"' 
ESCAPED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(ID_EMPLOI,TITRE_EMPLOI,SALAIRE_MINIMUM,SALAIRE_MAXIMUM);

-- HISTORIQUE EMPLOI ----------------------------------------- 
TRUNCATE TABLE historique_emploi; 

LOAD DATA INFILE 'C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\HISTORIQUE_EMPLOIS.CSV'
INTO TABLE historique_emploi
COLUMNS TERMINATED BY ';' 
OPTIONALLY ENCLOSED BY '"' 
ESCAPED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(ID_EMPLOYE,DATE_DEBUT_EMPLOI,@DATE_FIN_EMPLOI,ID_EMPLOI,ID_DEPARTEMENT,SALAIRE)

-- Conditions pour importer les dates de fin d'emploi selon leur forme ou si elles sont nulles
SET DATE_FIN_EMPLOI = CASE
						WHEN @DATE_FIN_EMPLOI = '' OR @DATE_FIN_EMPLOI IS NULL
                        THEN STR_TO_DATE(DATE_ADD(CURRENT_DATE, INTERVAL 10 YEAR), '%Y-%m-%d')
						WHEN SUBSTRING(@DATE_FIN_EMPLOI, 5, 1) = '.'
                        THEN STR_TO_DATE(@DATE_FIN_EMPLOI, '%Y.%m.%d')
                        ELSE STR_TO_DATE(@DATE_FIN_EMPLOI, '%Y-%m-%d')
                        END;

SET FOREIGN_KEY_CHECKS = 1;

-- -----------------------------------
-- PARTIE 3 — DML : requêtes demandées
-- -----------------------------------

-- Requête 1 à 4 effectué par Nicolas
-- 1) Retrouver et afficher les informations sur les bureaux d’un pays donné si on connait son ID (ex. JP)
SELECT *
	FROM bureau
    WHERE ID_PAYS = 'JP';
    
-- 2) Retrouver et afficher les informations sur les bureaux triés selon le ID_PAYS.
SELECT *
	FROM bureau
    ORDER BY ID_PAYS;
    
-- 3) Afficher le ID_PAYS, le nom du pays et le nombre de bureaux de chaque pays.
SELECT
	p.ID_PAYS AS 'ID pays'
    ,p.NOM_PAYS AS 'Nom pays'
    ,COUNT(b.ID_BUREAU) AS 'Nombre de bureaux'
    FROM pays AS p
    INNER JOIN bureau AS b
    ON p.ID_PAYS = b.ID_PAYS
    GROUP BY p.ID_PAYS;
    
-- 4) Afficher le ID_EMPLOI et le TITRE_EMPLOI de l’emploi qui a le plus cher salaire selon salaire_maximum.
SELECT
	ID_EMPLOI AS 'ID Emploi'
    ,TITRE_EMPLOI AS 'Titre emploi'
    ,MAX(SALAIRE_MAXIMUM) AS 'Salaire le plus élevé'
    FROM emploi;
    
-- Requête 5 à 8 effectué par Tristan  
-- 5) Afficher le salaire minimum moyen et le salaire maximum moyen des emplois de la compagnie.
SELECT
    ROUND(AVG(SALAIRE_MINIMUM), 2) AS 'Salaire minimum moyen'
    ,ROUND(AVG(SALAIRE_MAXIMUM), 2) AS 'Salaire maximum moyen'
    FROM emploi;
    
-- 6) Afficher le ID_REGION et le nombre de pays de chaque région
SELECT
	r.ID_REGION AS 'ID Région'
    ,COUNT(p.ID_PAYS) AS 'Nombre de pays dans la région'
    FROM region AS r
    INNER JOIN pays AS p
    ON r.ID_REGION = p.ID_REGION
    GROUP BY r.ID_REGION;
    
-- 7) Afficher le nom de la région et le nombre de pays de chaque région
SELECT
	r.NOM_REGION AS 'Région'
    ,COUNT(p.ID_PAYS) AS 'Nombre de pays dans la région'
    FROM region AS r
    INNER JOIN pays AS p
    ON r.ID_REGION = p.ID_REGION
    GROUP BY r.ID_REGION;
    
-- 8) Afficher le nom du pays et nombre de bureaux de chaque pays
SELECT
	p.NOM_PAYS AS 'Pays'
    ,COUNT(b.ID_BUREAU) AS 'Nombre de bureau dans le pays'
    FROM pays AS p
    INNER JOIN bureau AS b
    ON p.ID_PAYS = b.ID_PAYS
    GROUP BY p.ID_PAYS;
    
-- Tables temporaires
-- Création de la table temporaire effectué par Nicolas
CREATE TEMPORARY TABLE tbl_tmp_burpays(
	NOM_PAYS VARCHAR(30) NOT NULL
	,ID_BUREAU INT NOT NULL PRIMARY KEY
    ,ADRESSE_RUE_BUREAU VARCHAR(30) NOT NULL
    ,CODE_POSTAL_BUREAU VARCHAR(12) NULL DEFAULT NULL
    ,NOM_VILLE_BUREAU VARCHAR(60) NOT NULL
    ,COURRIEL_BUREAU VARCHAR(60) NOT NULL
)ENGINE = MEMORY;

INSERT INTO tbl_tmp_burpays
	SELECT p.NOM_PAYS
			,b.ID_BUREAU
            ,b.ADRESSE_RUE_BUREAU
            ,b.CODE_POSTAL_BUREAU
            ,b.NOM_VILLE_BUREAU
            ,b.COURRIEL_BUREAU
            FROM bureau AS b
            INNER JOIN pays AS p
            ON b.ID_PAYS = p.ID_PAYS
            ORDER BY p.NOM_PAYS;


-- VUES -------------------------------------------------------------------
-- Création de vue 1 à 4 effectué par Nicolas
-- Afficher le nom de la région et le nom des pays de chaque région
CREATE VIEW Vue1 AS
SELECT r.NOM_REGION, p.NOM_PAYS
FROM region as r
	INNER JOIN pays as p
		ON r.ID_REGION = p.ID_REGION
ORDER BY r.NOM_REGION;

-- Afficher le nom des régions qui possède plus que 3 pays.
CREATE VIEW Vue2 AS
SELECT r.NOM_REGION
FROM region as r
	INNER JOIN pays as p
		ON r.ID_REGION = p.ID_REGION
GROUP BY r.NOM_REGION
HAVING COUNT(P.ID_REGION) > 3
ORDER BY r.NOM_REGION;


-- Afficher pour chaque employé, son nom, prénom, son titre d’emploi actuel, le nom de son département, son salaire et la date de début de son emploi actuel. 
CREATE VIEW Vue3 AS
SELECT  employe.NOM_EMPLOYE, employe.PRENOM_EMPLOYE, 
		emploi.TITRE_EMPLOI, 
		departement.NOM_DEPARTEMENT, 
        historique_emploi.SALAIRE, historique_emploi.DATE_DEBUT_EMPLOI
FROM employe
INNER JOIN historique_emploi ON  employe.ID_EMPLOYE = historique_emploi.ID_EMPLOYE
	INNER JOIN emploi  ON historique_emploi.ID_EMPLOI = emploi.ID_EMPLOI
		INNER JOIN departement ON historique_emploi.ID_DEPARTEMENT = departement.ID_DEPARTEMENT
WHERE historique_emploi.DATE_FIN_EMPLOI IS NULL OR historique_emploi.DATE_FIN_EMPLOI > CURRENT_DATE();


-- Afficher le nom et le salaire moyen des emplois actuels de chaque département
CREATE VIEW Vue4 AS
SELECT  emploi.TITRE_EMPLOI AS "Emploi", 
        ROUND(AVG(historique_emploi.SALAIRE), 2) AS "Salaire Moyen"
FROM emploi
INNER JOIN historique_emploi ON emploi.ID_EMPLOI = historique_emploi.ID_EMPLOI
WHERE historique_emploi.DATE_FIN_EMPLOI IS NULL OR historique_emploi.DATE_FIN_EMPLOI > CURRENT_DATE()
GROUP BY emploi.TITRE_EMPLOI;

 -- Requête 5 à 9 effectué par Tristan
-- Afficher le nom et la masse salariale de chaque département (les emplois actuels)
CREATE VIEW Vue5 AS
SELECT NOM_DEPARTEMENT as "Departement",
	   SUM(historique_emploi.SALAIRE) AS "Masse Salariale"
FROM historique_emploi
	INNER JOIN departement on departement.ID_DEPARTEMENT = historique_emploi.ID_DEPARTEMENT
WHERE historique_emploi.DATE_FIN_EMPLOI IS NULL OR historique_emploi.DATE_FIN_EMPLOI > CURRENT_DATE()
GROUP BY departement.NOM_DEPARTEMENT;


-- Afficher le nom et la masse salariale de chaque bureau (les emplois actuels)
CREATE VIEW Vue6 AS
SELECT  Bureau.NOM_VILLE_BUREAU as "Nom du bureau",
		SUM(historique_emploi.SALAIRE) AS "Masse Salariale"
FROM Bureau
INNER JOIN departement ON Bureau.ID_BUREAU = departement.ID_BUREAU
	INNER JOIN historique_emploi ON departement.ID_DEPARTEMENT = historique_emploi.ID_DEPARTEMENT
WHERE historique_emploi.DATE_FIN_EMPLOI IS NULL OR historique_emploi.DATE_FIN_EMPLOI > CURRENT_DATE()
GROUP BY Bureau.NOM_VILLE_BUREAU;


-- Afficher le nom des employés qui ont occupé 3 emplois incluant celui qu’ils occupent encore.
CREATE VIEW Vue7 AS
SELECT  DISTINCT CONCAT(employe.PRENOM_EMPLOYE, " ", employe.NOM_EMPLOYE) as "Nom"
FROM employe
INNER JOIN historique_emploi ON employe.ID_EMPLOYE = historique_emploi.ID_EMPLOYE
WHERE employe.ID_EMPLOYE = ANY (SELECT ID_EMPLOYE FROM historique_emploi GROUP BY ID_EMPLOYE HAVING COUNT(ID_EMPLOYE) >=3)
ORDER BY employe.PRENOM_EMPLOYE;


-- Afficher le nom et le prénom de l’employé qui a changé le plus d’emploi.
CREATE VIEW Vue8 AS
SELECT  DISTINCT CONCAT(employe.PRENOM_EMPLOYE, " ", employe.NOM_EMPLOYE) as "Nom"
FROM employe
INNER JOIN historique_emploi ON employe.ID_EMPLOYE = historique_emploi.ID_EMPLOYE
WHERE employe.ID_EMPLOYE = (SELECT ID_EMPLOYE FROM historique_emploi GROUP BY ID_EMPLOYE ORDER BY COUNT(ID_EMPLOYE) DESC LIMIT 1)
ORDER BY employe.PRENOM_EMPLOYE;


-- Afficher le nom (sous forme Nom, Prénom) de chaque employé ainsi que le nom (sous forme Nom, prénom) de son responsable. 
CREATE VIEW Vue9 AS
SELECT CONCAT(parent.NOM_EMPLOYE, " ", parent.PRENOM_EMPLOYE) AS "Employe",
	   CONCAT(employe.NOM_EMPLOYE, " ",employe.PRENOM_EMPLOYE) AS "Responsable"
FROM employe
LEFT JOIN employe AS parent ON employe.ID_EMPLOYE = parent.RESPONSABLE_EMPLOYE
WHERE parent.RESPONSABLE_EMPLOYE = employe.ID_EMPLOYE 
ORDER BY Parent.RESPONSABLE_EMPLOYE;
